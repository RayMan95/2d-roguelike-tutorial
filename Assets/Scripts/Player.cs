﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObject
{
    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float restartLevelDelay = 1f;

    public Text foodText;

    private Animator animator;
    private int food;

    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;	
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;

    // Start is called before the first frame update
    protected override void Start()
    {
        animator = GetComponent<Animator>();

        food = GameManager.instance.playerFoodPoints;
        foodText.text = "Food: " + food;

        base.Start();
    }

    private void OnDisable()
    {
        GameManager.instance.playerFoodPoints = food;
    }

    private void OnTriggerEnter2D (Collider2D other)
    {
        if (other.tag == "Exit")
        {
            Debug.Log("Collision: exit");
            Invoke("Restart", restartLevelDelay);
            enabled = false;
        }
        else if (other.tag == "Food")
        {
            Debug.Log("Collision: food");
            food += pointsPerFood;
            foodText.text = "+" + pointsPerFood;
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Soda")
        {
            Debug.Log("Collision: soda");
            food += pointsPerSoda;
            foodText.text = "+" + pointsPerSoda;
            other.gameObject.SetActive(false);
        }
    }

    protected override bool AttemptMove<T>(int xDir, int yDir)
    {
        if (base.AttemptMove<T>(xDir,yDir))
        {
            food--;
            foodText.text = "Food: " + food;
            SoundManager.instance.RandomizeSfx (moveSound1, moveSound2);
        }
        
        RaycastHit2D hit;
        Move (xDir, yDir, out hit);
        CheckIfGameOver();

        GameManager.instance.playersTurn = false;
        return true;
    }

    private void CheckIfGameOver()
    {
        Debug.Log("Food: " + food);
        if (food <= 0)
            GameManager.instance.GameOver();
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.playersTurn)
            return;

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
            vertical = 0;
        
        if (horizontal != 0 || vertical != 0)
            AttemptMove<Wall> (horizontal, vertical);

    }

    protected override void OnCantMove<T>(T component)
    {
        Wall hitWall = component as Wall;

        hitWall.DamageWall(wallDamage);
        animator.SetTrigger("PlayerChop");
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

    public void LoseFood(int loss)
    {
        animator.SetTrigger("Hit");

        foodText.text = "-" + loss;
        food -= loss;

        CheckIfGameOver();
    }
}
