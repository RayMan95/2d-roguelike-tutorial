Unity game made following the 2D Roguelike tutorial (https://learn.unity.com/tutorial/2d-roguelike-setup-and-assets/?projectId=5c514a00edbc2a0020694718)

Most image assets are not my property and almost everything else was made following the tutorial. Some things were my flavourful additions.